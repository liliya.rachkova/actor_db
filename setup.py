from setuptools import setup, find_packages

install_requires = [
    'SQLAlchemy==1.4.25',
    'pydantic_sqlalchemy>=0.0.8.post1'
]

setup(
    name='actor_db',
    version='0.0.5',
    description='DB model for actor project',
    packages=find_packages(),
    install_requires=install_requires
)
