from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base, as_declarative
from db_actor_model.settings import settings

connectionStr = 'postgresql://{user}:{password}@{host}:{port}/{name}'.format(
    user = settings.DB_USER,
    password = settings.DB_PASSWORD,
    host = settings.DB_HOST,
    port = settings.DB_PORT,
    name = settings.DB_NAME
)

engine = create_engine(connectionStr, echo = True, pool_pre_ping=True)
SessionLocal = sessionmaker(
    autocommit=True,
    bind=engine
)

@as_declarative()
class Base:
    pass

def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()

def get_db_local():
    try:
        db = SessionLocal()
        return db
    finally:
        db.close()

