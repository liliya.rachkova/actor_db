import datetime
from sqlalchemy import Column, Sequence, String, Integer, DateTime, CHAR, ForeignKey
from sqlalchemy.orm import relationship, backref
from db_actor_model.database import Base


class ActArtefacts(Base):
    __tablename__ = 'act_artefacts'

    artefact_id_seq = Sequence('artefact_id_seq', metadata=Base.metadata)
    act_artefact_id = Column(Integer, artefact_id_seq,
                         server_default=artefact_id_seq.next_value(),
                         primary_key=True,
                         nullable=False)
    act_artefact_address = Column(String, nullable=False)
    act_artefact_values = Column(String)
    act_artefact_state_id = Column(Integer,
                               ForeignKey("states.state_id"),
                               nullable=False)
    artefact_artefact_id = Column(String,
                               ForeignKey("artefacts.artefact_id"),
                               nullable=False)
    # Не помню, для чего он был, поэтому пока пусть будет с -1
    act_process_process_id = Column(Integer,
                                  ForeignKey("act_processes.act_process_id"),
                                  #nullable=False,
                                  default=None)

    start_date = Column(DateTime, nullable=False, default=datetime.datetime.now)
    end_date = Column(DateTime, nullable=False, default=datetime.datetime(5999, 12, 31, 23, 59, 59))

    act_tasks = relationship("ActTasks",
                             secondary='artefact_x_task',
                             lazy=False,
                             uselist=True,
                             overlaps='act_artefacts')
