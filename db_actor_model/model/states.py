import datetime
from sqlalchemy import Column, Sequence, String, Integer, DateTime, CHAR, ForeignKey
from sqlalchemy.orm import relationship, backref
from db_actor_model.database import Base


class States(Base):
    __tablename__ = 'states'

    state_id = Column(Integer, primary_key=True, nullable=False)
    state_desc = Column(String)

    act_tasks = relationship("ActTasks", lazy=False)
    act_processes = relationship("ActProcesses", lazy=False)
    act_artefacts = relationship("ActArtefacts", lazy=False)