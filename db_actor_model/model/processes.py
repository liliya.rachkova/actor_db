import datetime
from sqlalchemy import Column, Sequence, String, Integer, DateTime, CHAR, ForeignKey
from sqlalchemy.orm import relationship, backref
from db_actor_model.database import Base


class Processes(Base):
    __tablename__ = 'processes'

    process_id = Column(String, primary_key=True, nullable=False)
    process_desc = Column(String)

    act_processes = relationship("ActProcesses", lazy=False)