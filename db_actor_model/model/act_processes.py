import datetime
from sqlalchemy import Column, Sequence, String, Integer, DateTime, CHAR, ForeignKey
from sqlalchemy.orm import relationship, backref
from db_actor_model.database import Base


class ActProcesses(Base):
    __tablename__ = 'act_processes'

    process_id_seq = Sequence('process_id_seq', metadata=Base.metadata)
    act_process_id = Column(Integer, process_id_seq,
                         server_default=process_id_seq.next_value(),
                         primary_key=True,
                         nullable=False)
    act_process_address = Column(String, nullable=False)

    act_process_state_id = Column(Integer,
                               ForeignKey("states.state_id"),
                               nullable=False)
    process_process_id = Column(String,
                               ForeignKey("processes.process_id"),
                               nullable=False)

    start_date = Column(DateTime, nullable=False, default=datetime.datetime.now)
    end_date = Column(DateTime, nullable=False, default=datetime.datetime(5999, 12, 31, 23, 59, 59))

    act_tasks = relationship("ActTasks",
                            secondary='process_x_task',
                            lazy=False,
                            uselist=True,
                            overlaps='act_tasks')
