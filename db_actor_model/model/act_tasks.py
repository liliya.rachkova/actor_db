import datetime
from sqlalchemy import Column, Sequence, String, Integer, DateTime, CHAR, ForeignKey
from sqlalchemy.orm import relationship, backref
from db_actor_model.database import Base


class ActTasks(Base):
    __tablename__ = 'act_tasks'

    task_id_seq = Sequence('task_id_seq', metadata=Base.metadata)
    act_task_id = Column(Integer, task_id_seq,
                         server_default=task_id_seq.next_value(),
                         primary_key=True,
                         nullable=False)
    act_task_address = Column(String, nullable=False)

    act_task_state_id = Column(Integer,
                               ForeignKey("states.state_id"),
                               nullable=False)
    task_task_id = Column(String,
                               ForeignKey("tasks.task_id"),
                               nullable=False)

    start_date = Column(DateTime, nullable=False, default=datetime.datetime.now)
    end_date = Column(DateTime, nullable=False, default=datetime.datetime(5999, 12, 31, 23, 59, 59))

    #act_artefacts = relationship("ActArtefacts",
    #                     secondary='artefact_x_task',
    #                     lazy=False,
    #                     uselist=True,
    #                     overlaps='act_tasks')
