import datetime
from sqlalchemy import Column, Sequence, String, Integer, DateTime, CHAR, ForeignKey
from sqlalchemy.orm import relationship, backref
from db_actor_model.database import Base


class LinksArtefactsXTasks(Base):
    __tablename__ = 'links_artefacts_x_tasks'

    task_task_id = Column(Integer, ForeignKey("tasks.task_id"),primary_key=True, nullable=False)
    required_artefact = Column(Integer, ForeignKey("artefacts.artefact_id"),primary_key=True, nullable=False)

