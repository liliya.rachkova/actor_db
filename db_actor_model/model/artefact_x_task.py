import datetime
from sqlalchemy import Column, Sequence, String, Integer, DateTime, CHAR, ForeignKey
from sqlalchemy.orm import relationship, backref
from db_actor_model.database import Base


class ArtefactXTask(Base):
    __tablename__ = 'artefact_x_task'

    act_artefact_artefact_id =Column(Integer, ForeignKey("act_artefacts.act_artefact_id"),primary_key=True, nullable=False)
    act_task_task_id = Column(Integer, ForeignKey("act_tasks.act_task_id"),primary_key=True, nullable=False)

    start_date = Column(DateTime, nullable=False, default=datetime.datetime.now)
    end_date = Column(DateTime, nullable=False, default=datetime.datetime(5999, 12, 31, 23, 59, 59))
