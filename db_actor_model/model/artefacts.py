import datetime
from sqlalchemy import Column, Sequence, String, Integer, DateTime, CHAR, ForeignKey
from sqlalchemy.orm import relationship, backref
from db_actor_model.database import Base


class Artefacts(Base):
    __tablename__ = 'artefacts'

    artefact_id = Column(String, primary_key=True, nullable=False)
    artefact_desc = Column(String)

    act_artefacts = relationship("ActArtefacts", lazy=False)