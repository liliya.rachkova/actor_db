import datetime
from sqlalchemy import Column, Sequence, String, Integer, DateTime, CHAR, ForeignKey
from sqlalchemy.orm import relationship, backref
from db_actor_model.database import Base


class Tasks(Base):
    __tablename__ = 'tasks'

    task_id = Column(String, primary_key=True, nullable=False)
    task_desc = Column(String)
    priority = Column(Integer, nullable=False)
    process_process_id = Column(String,
                                ForeignKey("processes.process_id"),
                                primary_key=True,
                                nullable=False)

    required_artefacts = relationship("Artefacts",
                            secondary= 'links_artefacts_x_tasks',
                            lazy=False,
                            uselist=True)
    act_tasks = relationship("ActTasks", lazy=False)

