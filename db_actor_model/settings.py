from pydantic import BaseSettings

class Settings(BaseSettings):

    DB_USER: str
    DB_PASSWORD: str
    DB_HOST: str
    DB_PORT: str
    DB_NAME : str

    class Config:
        case_sensitive = True
        env_file = ".env"


settings = Settings()
